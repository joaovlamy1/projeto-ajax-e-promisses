const url = "https://treinamentoajax.herokuapp.com/messages"

//MVP 1

//declarando espaços do formulario
const nomeform = document.getElementById("nomeform")
const msgform = document.getElementById("msgform")
const lista = document.getElementById("lista")

// função assincrona que posta formulario
async function post(){
    let nome = nomeform.value
    let msg = msgform.value

    const fetchBody = {
     "message":{
         "name": nome,
         "message": msg
        }
    }
    const fetchConfig = {
     method: "POST",
     headers: {"Content-Type":"application/JSON"},
     body: JSON.stringify(fetchBody)
    }

    await fetch( url , fetchConfig)

    nomeform.value = ""
    msgform.value = ""
}

function obt(){
    lista.innerHTML = ""

    fetch(url)
    .then(response => response.json())
    .then(response => {
        for (i in response){
            let div = document.createElement("div");
            let h3 = document.createElement("h3");
            let p =  document.createElement("p");
            var btn = document.createElement("button")

            h3.innerHTML = response[i].name;
            p.innerHTML = response[i].message;
            btn.innerText = "deletar"
            
            div.appendChild(h3);
            div.appendChild(p);
            div.id = response[i].id;
            btn.id = i
            div.appendChild(btn)

            lista.appendChild(div);

            

            let btnid = document.getElementById(i);
            console.log(btnid)
            btnid.addEventListener("click", e =>{
                fetch(url + "/"+ response[btnid.id].id , {
                    method:"DELETE"
                });
                let d = document.getElementById(response[btnid.id].id)
                d.innerHTML = "Deletado"
            })


            
        }
    });
}


// MVP 2
const inputid = document.getElementById("id")

function obtes(){
    lista.innerHTML = ""
    let id = inputid.value

    fetch(url + "/" + id)
    .then(response => response.json())
    .then(response => {
            let div = document.createElement("div");
            let h3 = document.createElement("h3");
            let p =  document.createElement("p");
            var btn = document.createElement("button")

            h3.innerHTML = response.name;
            p.innerHTML = response.message;
            btn.innerText = "deletar"
            
            div.appendChild(h3);
            div.id = id
            div.appendChild(p);
            div.appendChild(btn)

            btn.addEventListener("click", e =>{
                fetch(url + "/"+ id , {
                    method:"DELETE"
                });
                let d = document.getElementById(id)
                d.innerHTML = "Deletado"
            })

            lista.appendChild(div);
        
    });
}